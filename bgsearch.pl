#!/usr/bin/perl


#################################################
# BGSEARCH.PL - by Shine Upon Thee

# This script is useful for quickly listing board games in your collection.
#
# You can choose a number of players to filter by, and narrow the results
# down by either the BGG community recommends, or just what is best with
# the chosen player count. Or you can see everything.
#
# You can sort the results in various ways, including alphabetical, the
# rank on BGG, the average rating, the Geek rating, your personal rating,
# release year, and more.
# 
# You can display more or less or all results. It aims to be flexible and easy
# to use, without having to memorize a bunch of commands.


#################################################
# TUTORIAL AND EXAMPLES

# For example, if I have five players wanting to play a game, I will want
# to get a list of all of the games in my collection which are considered
# best at 5 players. To do this, I would just run:
#       ./bgsearch.pl best 5
#
# If the results are too few, I might want to expand it to include games that
# are recommended for 5 players, and not necessarily best at 5.
#       ./bgsearch.pl rec 5
# Note that you do not have to remember the short-form "rec", you could type
# in "recommend" or "recommended" and it will still work.
#
# If there are still too few results, I can just get a list of all games that
# support 5 players.
#       ./bgsearch 5
#
# Of course, the first thing you need to do is download your exported collection
# from BoardGameGeek, then call the script with it. Do this any time that you
# add some games to your collection or just want the latest data.
#       ./bgsearch.pl collection.csv
# Note: If you want to download your collection without expansions, you need to
# do it manually by visiting this URL with your username:
# https://www.boardgamegeek.com/geekcollection.php?action=exportcsv&subtype=boardgame&excludesubtype=boardgameexpansion&username=YOURUSERNAMEHERE
#
# OK, let's get back to the examples.
#
# Let's say that I had a big selection with just the recommended games for 5
# players. I could then sort them by my personal rating, or by the BGG rank,
# or by the BGG average user rating. To do these sorts, respectively:
#       ./bgsearch.pl rec 5 rating
#       ./bgsearch.pl rec 5 rank
#       ./bgsearch.pl rec 5 average
#
# Let's say we're looking for a heavy game to play. We could search all the
# games which support 5 players and sort them by heaviness. Likewise, we could
# reverse it and search for the lighter games. The weight isn't currently displayed
# but the sort will apply. There are other aliases for these (simple, complex, etc).
#       ./bgsearch.pl 5 heavy
#       ./bgsearch.pl 5 light
#
# If the results appear to be cut off, I could double the amount of results displayed.
# Or we can display all of the results in the collection. Or if too much is displayed
# by default, we could cut the results in half.
#       ./bgsearch.pl 5 more
#       ./bgsearch.pl 5 all
#       ./bgsearch.pl 5 less
#
# Another handy sort is random, and we can change the output format from the default
# to double-line or back to single-line if you've edited the default in this script.
#       ./bgsearch.pl 5 random
#       ./bgsearch.pl 5 multi
#       ./bgsearch.pl 5 single
#
# You can discover or expand all the options by looking at and editing the code below!

# required this on my Debian install:
#       sudo apt install libclass-csv-perl libmath-round-perl 



#################################################
# TWEAKABLE VARIABLES 

# this is the path to the collection file to grep
my $file = "/home/you/.bggcollection.csv";

# number of games to list by default
my $limit = 20;

# default output format is single-line
my $format = 1;


#################################################
# CODE

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(UTF-8)';
use Text::CSV_XS qw( csv );
use File::Copy qw( move );
use List::Util qw ( shuffle );
use Term::ANSIColor;
use Math::Round;
use Data::Dumper;

my $sort = "";
my $revsort = "";
my $players = "";
my $filter = "";

# parse the arguments
for my $arg (@ARGV) {
        # update collection file
        if ( $arg =~ /\.csv$/ ) {
                move ($arg, $file) or die("Unable to update BGG Collection file. $!\n");
                print "BGG Collection file updated.\n";
        }
        # randomize sort
        elsif ($arg =~ /^rand/) {
                $sort = "random";
        }
        # sort by alphabetic
        elsif ($arg =~ /^alph|^nam|^titl/) {
                $sort = "objectname";
        }
        # sort by rank
        elsif ($arg =~ /^rank/) {
                $sort = "rank";
        }
        # sort by my rating
        elsif ($arg =~ /^rat/) {
                $sort = "rating";
                $revsort = 1;
        }
        # sort by avg rating
        elsif ($arg =~ /^av/) {
                $sort = "average";
                $revsort = 1;
        }
        # sort by geek rating
        elsif ($arg =~ /^geek/) {
                $sort = "baverage";
                $revsort = 1;
        }
        # sort by release year
        elsif ($arg =~ /^rel|^date|^year|^pub|^new/) {
                $sort = "yearpublished";
                $revsort = 1;
        }
        # sort by oldest
        elsif ($arg =~ /^old/) {
                $sort = "yearpublished";
        }
        # sort by length
        elsif ($arg =~ /^time|^leng|^minu|^play|^long/) {
                $sort = "playingtime";
                $revsort = 1;
        }
        # sort by shortest
        elsif ($arg =~ /^shor/) {
                $sort = "playingtime";
        }
        # sort by weight
        elsif ($arg =~ /^weight|^lig|^lite|^simp/) {
                $sort = "avgweight";
        }
        # sort by weight (heaviest)
        elsif ($arg =~ /^heav|^complex|^diff/) {
                $sort = "avgweight";
                $revsort = 1;
        }
        # filter only best with X players
        elsif ($arg =~ /^best/) {
                $filter = "best";
        }
        # filter only recommended with X players
        elsif ($arg =~ /^rec/) {
                $filter = "rec";
        }
        # display less results
        elsif ($arg =~ /^les|^few|^half/) {
                $limit /= 2;
        }
        # display more results
        elsif ($arg =~ /^mor|^lot/) {
                $limit *= 2;
        }
        # display unlimited results
        elsif ($arg =~ /^unl|^all/) {
                $limit = 99999999;
        }
        # player count filter
        elsif ($arg =~ /^[0-9]+$/) {
                $players = $arg;
        }
        # single-line format
        elsif ($arg =~ /^sin|^one|^wid/) {
                $format = 1;
        }
        # multi-line format
        elsif ($arg =~ /^doub|^two|^mul/) {
                $format = 2;
        }
}

# load the file into an array
die("BGG Collection file not found. Pass the full path to your collection.csv to this script.\n") if not -r $file;
my $bggcsv = csv (in => $file, headers => "auto");

my @games;
my $match;

# loop through and filter the games
foreach my $game (@{$bggcsv}) {

        # default sort will be:
        # best > recommended > supported

        my @p_all = ($game->{minplayers}..$game->{maxplayers});
        my $all_players = join(",",@p_all);
        if ($game->{bggbestplayers} =~ /\b$players\b/) {
                $game->{playersort} = 2;
        }
        elsif ($game->{bggrecplayers} =~ /\b$players\b/) {
                $game->{playersort} = 1;
        }
        else {
                $game->{playersort} = 0;
        }

        $match = 1;

        if ($filter eq "best") {
                $match = 0 if not $game->{bggbestplayers} =~ /\b$players\b/;
        }
        elsif ($filter eq "rec") {
                $match = 0 if not $game->{bggrecplayers} =~ /\b$players\b/;
        }
        elsif (not $players) {
                $match = 1;
        }
        else {
                $match = 0 if not $all_players =~ /\b$players\b/;
        }

        push @games, $game if $match;
}

# now sort them
my @games_sorted;

if ($revsort) {
        @games_sorted = sort { $b->{$sort} <=> $a->{$sort} } @games;
}
elsif ($sort and $sort eq "random") {
        @games_sorted = shuffle @games;
}
elsif ($sort) {
        @games_sorted = sort { $a->{$sort} <=> $b->{$sort} } @games;
}
else {
        # default sort, will sort matching the player counts with best first, recommended, then everything else
        @games_sorted = sort { $b->{playersort} <=> $a->{playersort} } @games;
}

# now display the final results

while (my ($index,$game) = each(@games_sorted)) {
        last if $index > $limit;
        # merge the player counts completely, with the best and recommended being colored differently
        my @p_all = ($game->{minplayers}..$game->{maxplayers});
        my $p_format = "";
        foreach my $p (@p_all) {
                $p_format .= color("yellow") if $game->{bggrecplayers} =~ /\b$p\b/;
                $p_format .= color("green") if $game->{bggbestplayers} =~ /\b$p\b/;
                $p_format .= "$p";
                $p_format .= color("reset") if ($game->{bggrecplayers} =~ /\b$p\b/ or $game->{bggbestplayers} =~ /\b$p\b/);
                if ($p eq "20") {
                        $p_format .= "+,";
                        last;
                }
                $p_format .=",";
        }

        chop($p_format);

        if ($format eq '1') {
                if ($game->{rank}) {
                        print "#";
                        print color("red");
                        print $game->{rank};
                }
                else {
                        print "n/a";
                }
                print color("reset bold");
                print "\t$game->{objectname} ";
                print color("reset");
                print "(";
                print $p_format;
                print ") - ";
                print color("bright_blue");
                print $game->{playingtime};
                print color("reset");
                print " min - ";
                if ($game->{rating}) {
                        print color("green bold");
                        print $game->{rating};
                        print color("reset");
                        print " vs ";
                }
                print nearest(0.1, $game->{average});
        }
        else {
                print color("reset bold");
                print "$game->{objectname} ";
                print color("reset");
                print "\n\t$p_format\n\t";
                if ($game->{rank}) {
                        print "#";
                        print color("red");
                        print $game->{rank};
                }
                else {
                        print "n/a";
                }
                print "\t";
                print color("bright_blue");
                print $game->{playingtime};
                print color("reset");
                print " min";
                print "\t";
                if ($game->{rating}) {
                        print color("green bold");
                        print $game->{rating};
                        print color("reset");
                        print " vs ";
                }
                print nearest(0.1, $game->{average});
        }

        print "\n";
}

1;

