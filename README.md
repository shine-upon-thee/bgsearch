# BGSEARCH.PL - by Shine Upon Thee

This script is useful for quickly listing board games in your collection.

You can choose a number of players to filter by, and narrow the results
down by either the BGG community recommends, or just what is best with
the chosen player count. Or you can see everything.

You can sort the results in various ways, including alphabetical, the
rank on BGG, the average rating, the Geek rating, your personal rating,
release year, and more.
 
You can display more or less or all results. It aims to be flexible and easy
to use, without having to memorize a bunch of commands.

![](screenshot.png)

## TUTORIAL AND EXAMPLES

For example, if I have five players wanting to play a game, I will want
to get a list of all of the games in my collection which are considered
best at 5 players. To do this, I would just run:
```
./bgsearch.pl best 5
```

If the results are too few, I might want to expand it to include games that
are recommended for 5 players, and not necessarily best at 5.
```
./bgsearch.pl rec 5
```
Note that you do not have to remember the short-form "rec", you could type
in "recommend" or "recommended" and it will still work.

If there are still too few results, I can just get a list of all games that
support 5 players.
```
./bgsearch 5
```

Of course, the first thing you need to do is download your exported collection
from BoardGameGeek, then call the script with it. Do this any time that you
add some games to your collection or just want the latest data.
```
./bgsearch.pl collection.csv
```
If you want to download your collection without expansions, you need to do it
manually by visiting this URL with your username:
https://www.boardgamegeek.com/geekcollection.php?action=exportcsv&subtype=boardgame&excludesubtype=boardgameexpansion&username=YOURUSERNAMEHERE

OK, let's get back to the examples.

Let's say that I had a big selection with just the recommended games for 5
players. I could then sort them by my personal rating, or by the BGG rank,
or by the BGG average user rating. To do these sorts, respectively:
```
./bgsearch.pl rec 5 rating
./bgsearch.pl rec 5 rank
./bgsearch.pl rec 5 average
```

Let's say we're looking for a heavy game to play. We could search all the
games which support 5 players and sort them by heaviness. Likewise, we could
reverse it and search for the lighter games. The weight isn't currently displayed
but the sort will apply. There are other aliases for these (simple, complex, etc).
```
./bgsearch.pl 5 heavy
./bgsearch.pl 5 light
```

If the results appear to be cut off, I could double the amount of results displayed.
Or we can display all of the results in the collection. Or if too much is displayed
by default, we could cut the results in half.
```
./bgsearch.pl 5 more
./bgsearch.pl 5 all
./bgsearch.pl 5 less
```

Another handy sort is random, and we can change the output format from the default
to double-line or back to single-line if you've edited the default in this script.
```
./bgsearch.pl 5 random
./bgsearch.pl 5 multi
./bgsearch.pl 5 single
```

You can discover or expand all the options by looking at and editing the code yourself!

Required this on my Debian install:
```
sudo apt install libclass-csv-perl libmath-round-perl 
```
